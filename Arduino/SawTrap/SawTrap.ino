#include "soc/rtc.h"

const int D = 16;
const int C = 17;
const int B = 18;
const int A = 19;

const int MOTOR = 27;

const int POWER = 13;

const int LD1 = 21;
const int LD2 = 22;
const int LD3 = 23;

const int IN1 = 34;
const int IN2 = 35;
const int IN3 = 32;
const int IN4 = 4;
const int IN5 = 25;
const int IN6 = 26;

bool in1ShouldBeConnected = false;
bool in1Connected = false;
bool in1HasBeenCut = false;
bool in2ShouldBeConnected = false;
bool in2Connected = false;
bool in2HasBeenCut = false;
bool in3ShouldBeConnected = false;
bool in3Connected = false;
bool in3HasBeenCut = false;
bool in4ShouldBeConnected = false;
bool in4Connected = false;
bool in4HasBeenCut = false;
bool in5ShouldBeConnected = false;
bool in5Connected = false;
bool in5HasBeenCut = false;
bool in6ShouldBeConnected = false;
bool in6Connected = false;
bool in6HasBeenCut = false;

int counterTime = 0;

const int binaryPin[4] = {A, B, C, D};

RTC_DATA_ATTR int bootCount = 0;
RTC_DATA_ATTR uint32_t previousWakeTime = 0;

bool startTimer = false;

void setup() {
  Serial.begin(115200);

  //Print the wakeup reason for ESP32
  print_wakeup_reason();

  //Increment boot number and print it every reboot
  ++bootCount;
  Serial.println("Boot number: " + String(bootCount));
  
  pinMode(A, OUTPUT);
  pinMode(B, OUTPUT);
  pinMode(C, OUTPUT);
  pinMode(D, OUTPUT);
  pinMode(LD1, OUTPUT);
  pinMode(LD2, OUTPUT);
  pinMode(LD3, OUTPUT);
  pinMode(MOTOR, OUTPUT);
  pinMode(POWER, OUTPUT);
  
  digitalWrite(LD1, HIGH);
  digitalWrite(LD2, HIGH);
  digitalWrite(LD3, HIGH);
  digitalWrite(A, LOW);
  digitalWrite(B, LOW);
  digitalWrite(C, LOW);
  digitalWrite(D, LOW);
  digitalWrite(LD1, LOW);
  digitalWrite(LD2, LOW);
  digitalWrite(LD3, LOW);

  digitalWrite(MOTOR, LOW);
  digitalWrite(POWER, LOW);

  counterTime = 100;

  // Checking fake wires
  in1Connected = in1ShouldBeConnected = checkIfFakeWireIsConnected(IN1);
  Serial.println("In 1 connected: " + String(in1Connected) + ", " + String(in1ShouldBeConnected));
  in2Connected = in2ShouldBeConnected = checkIfFakeWireIsConnected(IN2);
  Serial.println("In 2 connected: " + String(in2Connected));
  in3Connected = in3ShouldBeConnected = checkIfFakeWireIsConnected(IN3);
  Serial.println("In 3 connected: " + String(in3Connected));
  in4Connected = in4ShouldBeConnected = checkIfFakeWireIsConnected(IN4);
  Serial.println("In 4 connected: " + String(in4Connected));
  in5Connected = in5ShouldBeConnected = checkIfFakeWireIsConnected(IN5);
  Serial.println("In 5 connected: " + String(in5Connected));
  in6Connected = in6ShouldBeConnected = checkIfFakeWireIsConnected(IN6);
  Serial.println("In 6 connected: " + String(in6Connected) + ", " + String(in6ShouldBeConnected));
  
  Serial.println("Setup done");

  //uint32_t timeSinceLast = ((uint32_t)rtc_time_get() / 150) - previousWakeTime; // 150kHz oscilator
  //Serial.println("Time since last wakeup ms: " + String(timeSinceLast));
  //delay(1000); 

  //digitalWrite(POWER, LOW);
}

bool checkIfFakeWireIsConnected(int inPin) {
  int read = analogRead(inPin);
  if (read >= 3500) {
    return true;
  }
  else {
    return false;
  }
}

void FIRE() {
  digitalWrite(POWER, HIGH);
  digitalWrite(MOTOR, HIGH);
  delay(1000);
  digitalWrite(POWER, LOW);
  digitalWrite(MOTOR, LOW);
}

void goToSleep() {
  digitalWrite(POWER, LOW);
  
  //Configure GPIO14 as ext0 wake up source for LOW logic level
  esp_sleep_enable_ext0_wakeup(GPIO_NUM_33, 1);

  //Go to sleep now
  esp_deep_sleep_start();
}

int calculateSpeedMultiplier() {
  // Checks fake wires
  // Multiplier is 2^(n*2), where n is the amount of cut wires.

  int cutWires = 0;

  // Checking which has been cut, if they were connected when started countdown. If they were cut before, they can't be reconnected
  in1HasBeenCut = in1HasBeenCut || (in1ShouldBeConnected == false || checkIfFakeWireIsConnected(IN1)) == false;
  cutWires = cutWires + int(in1HasBeenCut);
  in2HasBeenCut = in2HasBeenCut || (in2ShouldBeConnected == false || checkIfFakeWireIsConnected(IN2)) == false;
  cutWires = cutWires + int(in2HasBeenCut);
  in3HasBeenCut = in3HasBeenCut || (in3ShouldBeConnected == false || checkIfFakeWireIsConnected(IN3)) == false;
  cutWires = cutWires + int(in3HasBeenCut);
  in4HasBeenCut = in4HasBeenCut || (in4ShouldBeConnected == false || checkIfFakeWireIsConnected(IN4)) == false;
  cutWires = cutWires + int(in4HasBeenCut);
  in5HasBeenCut = in5HasBeenCut || (in5ShouldBeConnected == false || checkIfFakeWireIsConnected(IN5)) == false;
  cutWires = cutWires + int(in5HasBeenCut);
  in6HasBeenCut = in6HasBeenCut || (in6ShouldBeConnected == false || checkIfFakeWireIsConnected(IN6)) == false;
  cutWires = cutWires + int(in6HasBeenCut);

  return pow(2, cutWires * 2);
}

void loop() {
  if (startTimer) {
    digitalWrite(POWER, HIGH);
    writeNumber(counterTime);

    int multiplier = calculateSpeedMultiplier();
    Serial.println("Multiplier is: " + String(multiplier));
    delay(1000 / multiplier);
    --counterTime;
  
    // If time has passed
    if (counterTime < 0) {
      FIRE();
      delay(500);
      goToSleep();
    }
  }
  else {
    goToSleep();
  }
}

//Function that prints the reason by which ESP32 has been awaken from sleep
void print_wakeup_reason(){
  esp_sleep_wakeup_cause_t wakeup_reason;
  wakeup_reason = esp_sleep_get_wakeup_cause();
  switch(wakeup_reason)
  {
    case 1  : Serial.println("Wakeup caused by external signal using RTC_IO"); startTimer = true; break;
    case 2  : Serial.println("Wakeup caused by external signal using RTC_CNTL"); startTimer = true; break;
    case 3  : Serial.println("Wakeup caused by timer"); startTimer = false; break;
    case 4  : Serial.println("Wakeup caused by touchpad"); startTimer = false; break;
    case 5  : Serial.println("Wakeup caused by ULP program"); startTimer = false; break;
    default : Serial.println("Wakeup was not caused by deep sleep"); startTimer = false; break;
  }
}

void writeNumber(int number) {
  if (number >= 1000) {
    number = 999;
  }
  int firstNumber = number / 100;
  number = number - firstNumber * 100;
  int secondNumber = number / 10;
  number = number - secondNumber * 10;
  int thirdNumber = number;
  
  writeToCell(firstNumber, LD1);
  writeToCell(secondNumber, LD2);
  writeToCell(thirdNumber, LD3);
}

void writeToCell(int number, int LD) {
  int returnValue[4] = {0, 0, 0, 0};
  intToBin(number, returnValue);
  //Serial.println("Writing " + binStr + ", to LD " + LD);
  digitalWrite(LD, HIGH);
  int k = 0;
  for (int i = 3; i >= 0; --i) {
    //int binary = atoi(binStr.charAt(i));
    digitalWrite(binaryPin[k], returnValue[i]);
    ++k;
  }
  digitalWrite(LD, LOW);
}

void intToBin(int number, int returnValue[]) {
  
  int temp[4] = {0, 0, 0, 0};
  if (number >= 10 || number < 0) {
    returnValue[0] = 1;
    returnValue[1] = 1;
    returnValue[2] = 1;
    returnValue[3] = 1;
  }
  else {
    int i = 0;
    while (i < 4) {
      temp[i] = number % 2;
      number = number / 2;
      ++i;
    }
    
    int k = 0;
    for (int j = i - 1; j >= 0; --j) {
      returnValue[k] = temp[j];
      ++k;
    }
    
  }
}
